var socket = io.connect();
let myData = {};

$.getJSON("socket.json", (result) => {
	myData = result;
});

function makeID()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function setText(myInfo) {
	"use strict";
	try {
		if (myInfo) {
			$("#infolabel").text(myInfo)
		}
		$("#online").text("[ Online: " + myData.envstatus.admin + " ]")
		$("#points").text("[ Points: " + myData.userstatus.points + " ]");
		$("#remoteusers").text("[ Remote Users: " + myData.envstatus.connected + " ]");
	} catch(err) {
		let annoying = false;
	}
}

function theCountdown($fxele, $gfxele) {
	let fxlen = 0;
	let gfxlen = 0;

	var counter=setInterval(function() {
		if (myData.envstatus.fxcooldown !== false && fxlen == 0) {
			//set a countdown length
			fxlen=0;
			fxlen = moment(myData.envstatus.fxcooldown).diff(moment(), 'seconds');
			$fxele.css("color", "#ff5500")
		} else if (myData.envstatus.fxcooldown !== false && fxlen > 0) {
			//update text
			fxlen=fxlen-1;
			$fxele.text("[ Sounds Cooldown: " + fxlen + "s ]");
		} else if (fxlen <= 0 || myData.envstatus.fxcooldown == false) {
			myData.envstatus.fxcooldown = false;
			$fxele.text("[ Sounds Cooldown: None ]");
			$fxele.css("color", "#cccccc")
		}

		if (myData.envstatus.gfxcooldown !== false && gfxlen == 0) {
			//set a countdown length
			gfxlen=0;
			gfxlen = moment(myData.envstatus.gfxcooldown).diff(moment(), 'seconds');
			$gfxele.css("color", "#ff5500")
		} else if (myData.envstatus.gfxcooldown !== false && gfxlen > 0) {
			//update text
			gfxlen=gfxlen-1;
			$gfxele.text("[ GIFs Cooldown: " + gfxlen + "s ]");
		} else if (gfxlen <= 0 || myData.envstatus.gfxcooldown == false) {
			myData.envstatus.gfxcooldown = false;
			$gfxele.text("[ GIFs Cooldown: None ]");
			$gfxele.css("color", "#cccccc")
		}

	}, 1000); //1000 will run it every 1 second
}

$(document).ready(function () {
	let myName = "";
	let myID = null;

	setText('Connecting...');
	$("#fxc").text("[ Sounds Cooldown: None ]");
	$("#gifc").text("[ GIFs Cooldown: None ]");
	theCountdown($("#fxc"), $("#gifc"))

	socket.on('connect', function () {
		// if I am a nub
		if (!localStorage.LBRemoteUsername) {

			if (myID == null) {
				//create and store ID
				myID = makeID();
				localStorage.setItem('LBRemoteRoomID', myID);

				myData.route.senderappid = myID;
				myData.client.inroom = myID;
				socket.emit('init', myData);

			}
			//show some text to the user that they won't understand
			setText("Copy and Paste !connect " + myID + " into the stream chat of your choice.");

			//if I am a returning user
		} else {

			//grab username and ID
			myData.route.sendername = localStorage['LBRemoteUsername'];
			myName = myData.route.sendername;
			myData.route.senderappid = localStorage['LBRemoteRoomID'];
			myID = myData.route.senderappid;
			myData.client.authorized = true;
			myData.client.inroom = myID;

			//join room
			socket.emit('init', myData);

			//friendly message
			setText("Hi " + myName);
		}
	});

	socket.on('updateEnvStatus', (envObj) => {
		myData.envstatus = envObj;
	});

	socket.on('update', function (data) {

		switch (data.data.action) {

			case 'adminAccept':
				myData.route.sendername = data.data.messageobject.author;
				myName = myData.route.sendername;
				myData.client.authorized = true;
				localStorage.setItem('LBRemoteUsername', myName);
				localStorage.setItem('LBRemoteRoomID', myID);
				setText('You are authorized ' + myName + ' - have fun :D');
				break;

			case 'adminReject':
				setText('Request Refused');
				break;

			//defaults to whatever msg the admin wanted to share
			default:
				if (myData.client.authorized) {setText('oooo, an update?')};
				break;
		}

	});

	//commands that can only be run while auth'd

		$('body').on('click', '.fx', function() {
			// ensure we don't have a cooldown or that queuing is enabled
			if (myData.envstatus.queue || !myData.envstatus.fxcooldown && myData.client.authorized && myData.envstatus.admin) {
				//request permission from the boss
				myData.route.targetroom = "admin";
				myData.route.isbroadcast = false;
				myData.data.action = "fx"
				myData.data.messagetext = "!fx " + $(this).text()

				socket.emit('update', myData);
			} else {
				setText("There are several reasons why that's not working, the most likely of which is you.")
			}
		});
		$('body').on('click', '.gfx', function() {
			if (myData.envstatus.queue || !myData.envstatus.gfxcooldown && myData.client.authorized && myData.envstatus.admin) {
				//request permission from the boss
				myData.route.targetroom = "admin";
				myData.route.isbroadcast = false;
				myData.data.action = "gfx"
				myData.data.messagetext = "!gfx " + $(this).text()

				socket.emit('update', myData);
			} else {
				setText("There are several reasons why that's not working, the most likely of which is you.")
			}
		});

	//navigation bar
	$('body').on('click', '.navBtn', function() {
		$('.viewport').html("<ul class='flex-container'></ul>");
		switch ($(this).text()) {
			case 'Sounds':
				$.getJSON("fx.json", function(json) {
					for (var keys in json) {
						$('.flex-container').append("<li class='flex-item fx'>" + keys.split(" ")[1] + " [" + json[keys].cost + "]</li>")
					}
				});
				break;

			case 'Gifs':
				$.getJSON("gfx.json", function(json) {
					for (var keys in json) {
						$('.flex-container').append("<li class='flex-item gfx'>" + keys.split(" ")[1] + " [" + json[keys].cost + "]</li>")
					}
				});
				break;

			case 'Support':
				$('.flex-container').append("<h2>Hey thanks!</h2>")
				$('.flex-container').append("<p>I try to ensure that anyone who is kind enough to leave me a tip gets something in return.  That includes giveaways, custom supporter only sound effects, bonus points (100 per dollar!) as well as access to the supporter only giveaway!<br>")
				$('.flex-container').append("Ask me about it in chat, I'll be happy to clarify any questions you might have.</p>")
				$('.flex-container').append("<p><a href='https://goo.gl/MTeQJJ'>If you want to skip that and just get to the fat loot - Thanks very much</a>")
				break;

			case 'Reset Auth':
				localStorage.removeItem('LBRemoteUsername');
				localStorage.removeItem('LBRemoteRoomID');
				myData.client.authorized = false;
				setText('Auth Reset - hit that refresh button for a new code.');
				break;

			default:
				$('.flex-container').append("<li class='flex-item'>Coming Soon</li>")

		}

	});


});
