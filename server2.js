'use strict'

let express = require('express'),
	bodyParser = require('body-parser'),
	debug = require('debug')('io'),
	io = require('socket.io'),
	hubSend = require('./hubSend.js'),
	_ = require('lodash'),
	EventEmitter = require('events').EventEmitter;


let app = express();

class hubServer {
	constructor() {
		this.eventBus = new EventEmitter()
		this.hubSend = new hubSend(this.eventBus)
		//this.hubRecieve = new hubSend(this.eventBus)
		this.PORT = process.env.PORT || 8080;

		this.hubStart()
	}

	hubStart() {
		app.set('views', __dirname + '/views')
		app.use(express.static(__dirname + '/resources'))
		app.use(express.static(__dirname + '/views'))
		app.use(bodyParser.json())
		app.use(bodyParser.urlencoded({
				extended: true
		}))

		let io = require('socket.io').listen(app.listen(this.PORT))

		//initializes both hubSend and hubRecieve with the instance of IO
		//this.eventBus.emit('updateIO', io, (resolve, reject) => {
		//	if (resolve) { console.log ('[HUB] Instantiated IO, passed to users') }
		//	if (reject) {console.log ('[HUB] Instantiated IO, no one recieved ')}
		//});


	}



}


new(hubServer)
