/* maintains the global server environment.  Fancy works for it sends shit.
 */
"use strict";

let _ = require('lodash'),
	debug = require('debug')('remoteServer:Updater'),
	mainEnv = require('./resources/socket.json'),
	moment = require('moment');

class updater {
	constructor(eventbus) {
		this.eventBus = eventbus;
		this.adminID = null;

		this.bindEvents()
	}


	bindEvents() {
		this.eventBus.on('userJoin', (data, io, socket) => {
			this.addUser(data, io, socket)
		});

		this.eventBus.on('userLeave', (socket, io) => {
			this.removeUser(socket, io)
		});


		this.eventBus.on('anUpdate', (data, io, socket) => {
			debug('Event: anUpdate=>Several::', JSON.stringify(data))

			//broadcast timestamp for fxcooldown, client handles own clear
			if (data.envstatus.fxcooldown !== false && mainEnv.envstatus.fxcooldown == false) {
				this.setFXCooldown(data, io)
			}
			//broadcast timestamp for fxcooldown, client handles own clear
			if (data.envstatus.gfxcooldown !== false && mainEnv.envstatus.gfxcooldown == false) {
				this.setGFXCooldown(data, io)
			}

			this.emitToRoom('update', data, io, data.route.targetroom)
			if (data.route.targetroom !== 'update') {
				this.emitToRoom('updateEnvStatus', mainEnv.envstatus, io, 'update')
			}

		});
	}

	setGFXCooldown(data, io) {
		mainEnv.envstatus = data.envstatus;
		let len = moment(data.envstatus.gfxcooldown).diff(moment(), 'seconds');
		debug('Info: Setting cooldown for GFX', len);

		setTimeout(() => {
			debug('Info: Cooldown cleared for GFX')
			mainEnv.envstatus.gfxcooldown = false;
			this.emitToRoom('updateEnvStatus', mainEnv.envstatus, io, 'update')
		}, len * 1000);
	}

	setFXCooldown(data, io) {
		mainEnv.envstatus = data.envstatus;
		let len = moment(data.envstatus.fxcooldown).diff(moment(), 'seconds');
		debug('Info: Setting cooldown for FX', len);

		setTimeout(() => {
			debug('Info: Cooldown cleared for FX')
			mainEnv.envstatus.fxcooldown = false;
			this.emitToRoom('updateEnvStatus', mainEnv.envstatus, io, 'update')
		}, len * 1000);
	}

	emitToRoom(tag, data, io, room) {
		debug('Func: emitToRoom (tag, data, room)::', tag, JSON.stringify(data), room)
		io.in(room).emit(tag, data);
	}

	removeUser(socket, io) {
		debug('Event: userLeave=>removeUser(socket)')
		mainEnv.envstatus.connected--;

		//record admin ID to see if he is departing
		if (this.adminID == socket.client.id) {
			mainEnv.envstatus.admin = false;
			this.adminID = null;
			debug('Info: User was admin, departed.')
		}

		this.emitToRoom('updateEnvStatus', mainEnv.envstatus, io, 'update');
	}

	addUser(data, io, socket) {
		debug('Func: eventBus.on:userJoin=>addUser::', JSON.stringify(data))
		mainEnv.envstatus.connected++;

		//record admin ID
		if (data.route.sendername == 'admin') {
			mainEnv.envstatus.admin = true;
			this.adminID = socket.client.id
			debug('Info: User is admin, socket ID recorded and environment updated.')
		}
		//log the interaction and send update
		debug('Info: userJoin + ' + socket.client.id + ' / ' + data.route.sendername);
		this.emitToRoom('updateEnvStatus', mainEnv.envstatus, io, 'update');
	}

}


module.exports = updater;
