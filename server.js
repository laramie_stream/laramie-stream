'use strict';

var express = require('express'),
    bodyParser = require('body-parser'),
		debug = require('debug')('remoteServer:server'),
    io = require('socket.io'),
    _ = require('lodash'),
		app = express(),
		EventEmitter = require('events').EventEmitter,
		updater = require('./updater');

class WebServer {
	constructor() {
		this.type = 'WEB SERVER';
		this.eventBus = new EventEmitter();
		this.PORT = process.env.PORT || 8080;
		//this.PORT = 80;

		this.updater = new updater(this.eventBus);

		//TODO do we need these?
		this.socket = null;

		this.startServer();
	}

	startServer() {
		debug('Func: startServer()');
		app.set('views', __dirname + '/views');
		app.use(express.static(__dirname + '/resources'));
		app.use(express.static(__dirname + '/views'));
		app.use(bodyParser.json());
		app.use(bodyParser.urlencoded({
			extended: true
		}));

		debug('Action: starting socket');
		var io = require('socket.io').listen(app.listen(this.PORT));

		// connected event
		io.sockets.on('connection', (socket) => {
			debug('SocketEvent: connection:', socket.client.id);
			//socket will always join the update room
			socket.join('update');

			//user submits room, contains their generated ID
			socket.on('init', (socketdata) => {
				debug('SocketEvent: init', JSON.stringify(socketdata));
				// admin sends 'admin' room, everyone else sends myID
				socket.join(socketdata.client.inroom);

				debug('Info: emitting to eventBus');
				this.eventBus.emit('userJoin', socketdata, io, socket)
			});

			//update handler, broadcast happens if sent to the update room
			socket.on('update', (socketdata) => {
				debug('socketEvent: update(socketdata)::', JSON.stringify(socketdata));
				debug('Info: emitting to eventBus');
				this.eventBus.emit('anUpdate', socketdata, io, socket)
			});

			//handle disconnects
			socket.on('disconnect', () => {
				debug('socketEvent: disconnect');
				debug('Info: emitting to eventBus');
				this.eventBus.emit('userLeave', socket, io)
			});

			this.socket = socket;
		});

		this.bindRoutes();

	}

	bindRoutes() {
		app.get('/', function (req, res) {
			res.render('views/index.html');
		});
	}

}

new WebServer();
