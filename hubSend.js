"use strict";

let debug = require('debug')('hubSend');

class hubSend {
	constructor(eventBus) {
		this.eventBus = eventBus;

		this.bindEvents();
		debug('%s constructed', 'hubSend');

		//client says hey plz send
		this.hubBusEmitReply('test', 'testdata', function(reply) {
			debug('in the client request ' + reply)
		})
	}

	//client wants to emit
	//callback is clientfn
	//hub recieves, value 3 is the clientfn
	//hub emits, passes reference to clientfn
	//someone recieves, uses clientfn to reply

	//wrapper to send on both the eventbus and sockets, with a reply
	hubBusEmitReply(flag, data, fnReply) {
		debug('Emitting... hubEmitReply(' + flag + ' , ' + data + ', ' + fnReply + ')');
		this.eventBus.emit(flag, data, fnReply);
	}


	//wrapper to send on both the eventbus and sockets, without a reply
	hubBusEmit(flag, data) {
		debug('Func: hubEmit(' + flag + ', ' + data + ')');
		this.eventBus.emit(flag, data);
	}

	hubSocketEmitReply() {}
	hubSocketEmit() {}


	bindEvents() {
		this.eventBus.on('test', function(flag, data, fnReply) {
			debug('in the hub event')
			fnReply('some worker heard this')
			//this.hubEmitReply(flag, data, fnReply);
		});

		this.eventBus.on('testing', function (data, fn) {
			fn('we got data');
		});
		debug('done with bind events')
	}

}

module.exports = hubSend;