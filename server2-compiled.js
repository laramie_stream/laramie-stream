'use strict';

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

let express = require('express'),
    bodyParser = require('body-parser'),
    debug = require('debug')('io'),
    io = require('socket.io'),
    _ = require('lodash'),
    EventEmitter = require('events').EventEmitter,
    hubProcessor = require('./updater');

let app = express();

class hubServer {
	constructor() {
		this.eventBus = new EventEmitter();
		this.hubProcessor = new hubProcessor(this.eventBus);
		//this.hubSend = new hubSend(this.eventBus)
		//this.hubRecieve = new hubSend(this.eventBus)
		this.PORT = process.env.PORT || 8080;

		this.hubStart();
	}

	hubStart() {
		var _this = this;

		return _asyncToGenerator(function* () {
			app.set('views', __dirname + '/views');
			app.use(express.static(__dirname + '/resources'));
			app.use(express.static(__dirname + '/views'));
			app.use(bodyParser.json());
			app.use(bodyParser.urlencoded({
				extended: true
			}));

			let io = require('socket.io').listen(app.listen(_this.PORT));
			//initializes both hubSend and hubRecieve with the instance of IO
			yield _this.eventBus.emit('updateIO', io, function (resolve, reject) {
				if (resolve) {
					console.log('[HUB] Instantiated IO, passed to users');
				}
				if (reject) {
					console.log('[HUB] Instantiated IO, no one recieved ');
				}
			});
		})();
	}

}

new hubServer();

//# sourceMappingURL=server2-compiled.js.map